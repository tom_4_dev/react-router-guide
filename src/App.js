import React, { Component } from 'react';
import {
  BrowserRouter,
  Link,
  Route
} from 'react-router-dom';

import './App.css';

const Home = () => (
  <div>
    <h2>Home</h2>
  </div>
)

const About = () => (
  <div>
    <h2>About</h2>
  </div>
)

const Topic = ({ match }) => (
  <div>
    <h3>{match.params.topicId}</h3>
  </div>
)

const Topics = ({ match }) => (
  <div>
    <h3>Topics</h3>

    <ul>
      <li>
        <Link to={`${match.url}/learning`}>
          Learning with react routing.
        </Link>
      </li>
      <li>
        <Link to={`${match.url}/rendering`}>
          Rendering with react routing.
        </Link>
      </li>
      <li>
        <Link to={`${match.url}/fucking`}>
          Messed up with react routing.
        </Link>
      </li>
    </ul>

    <Route path={`${match.path}/:topicId`} component={Topic} />
  </div>
)

const App = () => (
  <BrowserRouter basename="/app">
    <div>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/about">About</Link></li>
        <li><Link to="/topics">Topics</Link></li>
      </ul>

      <hr />

      <Route exact path="/" component={Home} />
      <Route path="/about" component={About} />
      <Route path="/topics" component={Topics} />
    </div>
  </BrowserRouter>
)

export default App;
